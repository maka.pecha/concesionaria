package parcial2.pecha;

/**
 *
 * @author Maka
 */
public class Auto {
    private String dominio;
    private boolean nuevo;
    private double precio;
    private String marca;

    public Auto(String dominio, boolean nuevo, double precio, String marca) {
        this.dominio = dominio;
        this.nuevo = nuevo;
        this.precio = precio;
        this.marca = marca;
    }

    public String getDominio() {
        return dominio;
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public double getPrecio() {
        double valor = 0;
        if (isNuevo()) {
           if (this.precio < 500000){
               valor = this.precio - this.precio*0.1; //10%
           } else {
               valor = this.precio - this.precio*0.05; //5%
           }
        } else {
           valor = this.precio - this.precio*0.2; //20%
        } 
        return valor;
    }

    public String getMarca() {
        return marca;
    }
    
    public String toStringNuevo() {
        String nuevo= "";
        if (isNuevo()) {
            nuevo = "si";
        }else{
            nuevo = "no";
        }
        return nuevo;
    }

    @Override
    public String toString() {
        return "Dominio: " + dominio + ", Nuevo:" + toStringNuevo() + ", Precio Lista:" + precio + ", Precio Descuento: " + getPrecio() + ", Marca: " + marca;
    }
    
}
