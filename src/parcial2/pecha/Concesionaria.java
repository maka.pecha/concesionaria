package parcial2.pecha;

import java.util.ArrayList;

/**
 *
 * @author Maka
 */
public class Concesionaria {
    private ArrayList<Auto> autos; 
    
    public Concesionaria() {
        autos = new ArrayList();
    }

    public ArrayList<Auto> getAutos() {
        return autos;
    }
    
    public void agregarAuto(Auto a){
        autos.add(a); 
    }
    
    public double promedioPrecioNuevos(){
        double acumulador = 0;
        int contador = 0;
        for (Auto a : autos ) {
            if(a.isNuevo()){
                acumulador += a.getPrecio();
                contador++;
            }
        }
        if(contador==0) return 0;
        else return acumulador/contador;
    }
    
    public int cantidadUsadosPrecioMenor300Mil(){
        int contador = 0;
        for (Auto a : autos ) {
            if((!a.isNuevo()) && a.getPrecio() < 300000){
                contador++;
            }
        }
        return contador;
    }
}
